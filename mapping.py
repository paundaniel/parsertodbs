__author__ = 'daniel'

class CMapping():
    def __init__(self, product_fields, mapping):
        self._product_fields = product_fields
        self._mapping = {v['value']: {"value" : k , "type" : v['type']} for k, v in mapping.items()}

    def do_mapping(self, csv_product):
        converted_product = {}
        i=0
        for key in self._product_fields:
            if key in self._mapping:
                if self._mapping[key]['type'] == u"string":
                    converted_product[self._mapping[key]['value']] = unicode(csv_product[i].strip("'").decode("utf-8"))
                elif self._mapping[key]['type'] == u"float":
                    converted_product[self._mapping[key]['value']] = float(csv_product[i])
                elif self._mapping[key]['type'] == u"int":
                    converted_product[self._mapping[key]['value']] = int(csv_product[i])

            i += 1
        return converted_product
