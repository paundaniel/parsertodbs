from tornado import web, ioloop
import random

__author__ = 'daniel'


class GenerateId(web.RequestHandler):
    def get(self):
        self.write(str(random.randint(1,1000000000)))

app = web.Application([
    web.URLSpec('/idgen', GenerateId)
], autoreload=True)

app.listen(port=3011)
ioloop.IOLoop.current().start()