from string import split
from Checker import CChecker
from mapping import CMapping

__author__ = 'daniel'

class CParser:
    def __init__(self, shop_id, feed, shop_settings, timestamp, parser_config):
        self._shop_id = shop_id
        self._shop_settings = shop_settings
        self._data = feed
        self._timestamp = timestamp
        self._parser_config = parser_config
    
    def handler(self):
        if self._shop_settings['feedParams']['type'] == u"csv":
            fields = self._data.next()
            lets = CMapping(product_fields=fields,
                            mapping=self._shop_settings['feedParams']['mapping'])
            do = CChecker(self._shop_id, timestamp=self._timestamp, parser_config=self._parser_config)

            #must a try catch
            counter = 0
            for product in self._data:
                # 1. get product
                # 2. mapp product
                # 3. send product to checker
                if counter == 1000:
                    do.exec_partial_bulk()
                    counter = 0
                mapped_product = lets.do_mapping(csv_product=product)
                mapped_product["shop"] = int(self._shop_id)

                #do.custom_stuff() ex: miniprix category:[cat1, cat2, cat3, cat4]
                customize = CCustom_methods()
                final_product = getattr(customize, 'shop_{0}'.format(self._shop_id))(mapped_product)

                do.check_product(product=final_product)
                counter += 1


            do.thigs_after_product_checking()
            print do.get_parse_history()
            #################

class CCustom_methods:
    def shop_130(self, product):
        product['category'] = []
        for i in range(1,5):
            product['category'].append(int(product['category{0}'.format(i)]))
            del product['category{0}'.format(i)]

        sizes = product['size']
        del product['size']
        product['size'] = []
        for elem in split(sizes, ','):
            product['size'].append(str(elem))

        return product

    def shop_102(self, product):
        if product["price"] < product["oldprice"]:
            product["discountPrice"] = product["price"]
            product["price"] = product["oldprice"]
            del product["oldprice"]
        else:
            del product["oldprice"]

        categories = product["category"]
        product["category"] = []
        product["category"] = split(categories,",")
        product["category"] = [int(cat) for cat in product["category"]]
        return product



















