import hashlib
import json
import requests
import sys
import workerForMongo

sys.path.append("/home/daniel/app.vibetrace.com/vibetraceV4")
from utils.rediscache import RedisWritethrough
__author__ = 'daniel'

class CRedisEntry:
    def __init__(self, json_obj, product_id, shop_id):
        ENTRYPATH = "parser:product:{0}:{1}"

        self.value = hashlib.sha256(str(json_obj)).hexdigest()
        self.json_obj = json_obj
        self.shop_id = shop_id
        self.product_id = product_id
        self.key = ENTRYPATH.format(str(self.shop_id), str(self.product_id))


class CRedisHandler:
    def __init__(self, shop_id, parser_config):
        self._redis_conn = parser_config["redis_connection"]
        self._pipe = self._redis_conn.pipeline()
        self._mongo = workerForMongo.CMongoHandler(shop_id=shop_id, parser_config=parser_config)
        self._shop = shop_id
        self._parse_history = {"shop" : shop_id,
                               "inserted" : 0,
                               "updated" : 0,
                               "deleted" : 0,
                               }

    def pipe_exec(self):
        self._pipe.execute()
        self._pipe.reset()

    def redis_checking(self, redis_entry, timestamp):
        # get the key of product from redis
        db_product_value = self._redis_conn.get(redis_entry.key)



        #if the key does not exit, his value is NULL
        #
        #

        post = json.loads(redis_entry.json_obj)
        #Category checking

        cat_cheker = RedisWritethrough(redis=self._redis_conn,
                                                  mongo=self._mongo._connection)
        for cat in post["category"]:
            cat_cheker.set_mongo(database="test_database", collection="category")
            new_category = cat_cheker.read("cat:{0}:{1}".format(self._shop, cat),
                                                      {"shop": self._shop,
                                                       "name": str(cat)},
                                                      field="_id")
            if not new_category:
                # Need to insert in mongo & redis new category data
                path = "http://localhost:3011/idgen"
                response = requests.get(path)
                if response.status_code != 200:
                    return None
                _id = long(response.content)
                mongo_insert = {"_id": _id, "name": str(cat), "shop": self._shop}
                self._mongo.select_collection(collection="category")
                self._mongo.simple_insert(mongo_insert)
                cat_cheker.write_redis(
                    "cat:{0}:{1}".format(self._shop, cat),
                    _id)

        #INSERT
        if db_product_value == None:
            #daca nu este in redis, face inserare
            self._parse_history["inserted"] += 1
            self._pipe.set(redis_entry.key, redis_entry.value)

            post["_id"] = long(requests.get('http://localhost:3011/idgen').content)
            #must do cast to unixLike timestamp!!!!!!!
            post["createdOn"] = timestamp
            post["active"] = True
            post["viewed7"] = 0
            post["purchased7"] = 0
            self._mongo.insert_doc(shop=post['shop'],
                                   product_id=post['idInShop'],
                                   post=post)
        #if the key exists but his value is different by redis value
        #
        #
        #UPDATE
        elif db_product_value != redis_entry.value:
            # attatch an instruction. ie. set)
            self._parse_history["updated"] += 1
            self._pipe.set(redis_entry.key, redis_entry.value)


            post = json.loads(redis_entry.json_obj)
            #must do cast to unixLike timestamp!!!!!!!
            post["updatedOn"] = timestamp
            self._mongo.update_doc(shop_id=redis_entry.shop_id,
                                   product_id=redis_entry.product_id,
                                   new_post=post)

        self._pipe.sadd("shop:{0}:new_active_products".format(redis_entry.shop_id), str(redis_entry.product_id))


    def rename_active_products(self, shop_id):
        self._pipe.rename(str("shop:{0}:new_active_products".format(str(shop_id))),
                                 str("shop:{0}:original_active_products".format(str(shop_id))))

    def check_for_deleted_products(self, shop_id):
        #
        #
        #DELETE
        active_diff = self._redis_conn.sdiff("shop:{0}:original_active_products".format(str(shop_id)),
                                              "shop:{0}:new_active_products".format(str(shop_id)))
        for elem in list(active_diff):
            self._parse_history["deleted"] += 1
            self._pipe.delete("parser:product:{0}:{1}".format(str(shop_id), str(elem)))
            self._mongo.to_delete(shop_id=shop_id,
                                  product_id=int(elem))


    def get_parse_history(self):
        return self._parse_history

