from time import sleep

__author__ = 'daniel'


class CElasticHandler:
    def __init__(self, shop_id, parser_config):
        self._connection = parser_config["elastic_connection"]
        self.select_index(shop_id=shop_id)
        self._bulk_list = []

    def select_index(self, shop_id):
        self._shop_index = str(shop_id) + "_items"
        if not self._connection.indices.exists(self._shop_index):
            self._connection.indices.create(self._shop_index)
            sleep(0.1)


    def insert_doc(self, post):
        self._bulk_list.append({"index":{"_index":self._shop_index,
                                        "_type":"product",
                                        "_id":post["_id"] }})
        self._bulk_list.append(post)

    def update_doc(self, post):
        self._bulk_list.append({"update":{"_index":self._shop_index,
                                        "_type":"product",
                                        "_id":post["_id"] }})
        self._bulk_list.append({"doc" : post})

    def to_delete(self, post):
        post["active"] = False
        self._bulk_list.append({"update":{"_index":self._shop_index,
                                        "_type":"product",
                                        "_id":post["_id"] }})
        self._bulk_list.append({"doc" : post})

    def execute_bulk(self):
        #try-catch needed
        self._connection.bulk(body=self._bulk_list,
                              index=self._shop_index,
                              doc_type="product")
        self._bulk_list = []

