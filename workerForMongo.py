from copy import deepcopy
import datetime
import iso8601
import pytz
import workerForElastic

__author__ = 'daniel'


class CMongoHandler:
    def __init__(self, shop_id, parser_config):
        self._connection = parser_config["mongo_connection"]
        self._db = self._connection['test_database']
        self._collection = self._db['item']
        self._bulk = self._collection.initialize_ordered_bulk_op()
        self._elastic_handler = workerForElastic.CElasticHandler(shop_id=shop_id, parser_config=parser_config)

    def map_timestamp(self, _post):
        _post["createdOn"] = self.date2timestamp(_post.get("createdOn"))
        _post["updatedOn"] = self.date2timestamp(_post.get("updatedOn")) if _post.get("updatedOn") else _post["createdOn"]

    def insert_doc(self, shop, product_id, post):
        self.select_collection("item")
        _post = deepcopy(post)
        from_mongo = self._collection.find_one({"shop": shop, "idInShop": product_id})
        if from_mongo:
            # sa verific daca exista in produsul din mongo
            #boughtwith il preiau daca exista in mongo pentru al pune in noul produs
            #
            _post["viewed7"] = from_mongo.get("viewed7", 0)
            _post["purchased7"] = from_mongo.get("purchased7", 0)
            _post["updatedOn"] = _post.get("createdOn")
            _post["createdOn"] = from_mongo.get("createdOn")
            _post["_id"] = from_mongo["_id"]
            self._elastic_handler.update_doc(deepcopy(_post))
            del _post["_id"]
            self.map_timestamp(_post)
            _post["boughtWith"] = from_mongo.get("boughtWith", [])
            self._bulk.find({'shop': shop, 'idInShop': product_id}).upsert().replace_one(_post)
        else:
            _post["updatedOn"] = _post.get("createdOn")
            self._elastic_handler.insert_doc(deepcopy(_post))
            self.map_timestamp(_post)
            _post["boughtWith"] = _post.get("boughtWith", [])
            self._bulk.find({'shop': shop, 'idInShop': product_id}).upsert().replace_one(_post)

    def display(self):
        for post in self._collection.find():
            print post


    def update_doc(self, shop_id, product_id, new_post):
        _post = deepcopy(new_post)
        from_mongo = self._collection.find_one({"shop": shop_id, "idInShop": product_id})
        updated_time_elastic = _post["updatedOn"]
        created_time_elastic = self.mili2ISO(from_mongo.get("createdOn"))
        _post["updatedOn"] = self.date2timestamp(_post["updatedOn"])
        _post["viewed7"] = from_mongo.get("viewed7")
        _post["purchased7"] = from_mongo.get("purchased7")
        _post["createdOn"] = from_mongo.get("createdOn")
        _post["active"] = from_mongo.get("active", True)
        _post["boughtWith"] = from_mongo.get("boughtWith", [])
        self._bulk.find({"shop": shop_id,
                         "idInShop": product_id}).replace_one(_post)
        _post_copy = deepcopy(_post)
        _post_copy["_id"] = from_mongo["_id"]
        _post_copy["updatedOn"] = updated_time_elastic
        _post_copy["createdOn"] = created_time_elastic
        del _post_copy["boughtWith"]

        self._elastic_handler.update_doc(_post_copy)



    def to_delete(self, shop_id, product_id):
        self._bulk.find({"shop": shop_id,
                         "idInShop": product_id}).update_one({'$set': {'active': False}})
        from_mongo = self._collection.find_one({"shop": shop_id, "idInShop": product_id})
        if from_mongo:
            self._elastic_handler.to_delete(post=from_mongo)


    def execute_bulk(self):
        if self.get_bulk_len() is not 0:
            self._bulk.execute()
            self._bulk = self._collection.initialize_ordered_bulk_op()
            self._elastic_handler.execute_bulk()


    def get_bulk_len(self):
        return len(self._bulk._BulkOperationBuilder__bulk.ops)

    def select_collection(self, collection):
        self._collection = self._db[collection]

    def simple_insert(self, document):
        self._collection.insert(document)


    def date2timestamp(self, value, mili=False):
        if type(value) in [unicode, str, datetime.datetime]:
            if type(value) is not datetime.datetime:
                value = iso8601.parse_date(value)
            if not value.tzinfo:
                value = value.replace(tzinfo=pytz.utc)
            value = (value -
                     datetime.datetime(
                         1970, 1, 1, tzinfo=pytz.utc)
                     ).total_seconds()
        else:
            value /= float(1000)
        if mili:
            value = int(value * 1000)
        return value

    def mili2ISO(self, value, format="%Y-%m-%d"):
        return datetime.datetime.fromtimestamp(value)

