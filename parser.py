from datetime import datetime
import json
import time
from elasticsearch_dsl import connections
import pymongo
import redis
import ParserHandler
import csv
import urllib2

__author__ = 'daniel'


class CInitParser:
    """Class implemented to parse a Json file and to return a JsonObj from it."""

    def __init__(self, shop_id, shop_config_file, parser_config):
        cfg_file = open(shop_config_file)
        self._shop_config_file = json.load(cfg_file)
        self._shop_id = shop_id
        self._timestamp = datetime.now()
        file.close(cfg_file)
        self._parser_config = parser_config


    def start(self):
        if self._shop_config_file['feedParams']['type'] == u"csv":
            self.handle_csv()
        elif self._shop_config_file['feedParams']['type'] == u"json":
            self.handle_json()
        else:
            print "Unknown type of feed!"
            #+logging



    def handle_csv(self):
        #get data
        response = urllib2.urlopen(self._shop_config_file['feed'])
        data = csv.reader(response,
                          delimiter=str(self._shop_config_file['feedParams']['delimiter']) if "delimiter" in self._shop_config_file['feedParams'] else "",
                          quotechar=str(self._shop_config_file['feedParams']['quote']) if "quote" in self._shop_config_file['feedParams'] else "",
                          quoting=csv.QUOTE_ALL if self._shop_config_file['feedParams']['quote'] != u"" else csv.QUOTE_NONE,
                          skipinitialspace=True)
        parse_shop = ParserHandler.CParser(shop_id=self._shop_id,
                                           feed=data,
                                           shop_settings=self._shop_config_file,
                                           timestamp=self._timestamp,
                                           parser_config=self._parser_config)
        parse_shop.handler()




    def handle_json(self):
        pass





if __name__ == "__main__":
    startime = time.time()
    redis_conn = redis.Redis(host='localhost',
                             port=6379)
    mongo_conn = pymongo.MongoClient('localhost', 27017)
    elastic_conn = connections.connections.create_connection(hosts=[{"host": "172.16.10.1", "port": 3002}],
                                                                     timeout=20)
    config = {"redis_connection" : redis_conn,
              "mongo_connection" : mongo_conn,
              "elastic_connection" : elastic_conn}
    # try:
    #     print redis_conn.info()
    #     print mongo_conn.server_info()
    #     print elastic_conn.info
    # except redis.exceptions.ConnectionError, err:
    #     print "Redis connection error:"
    #     print err.message
    #     sys.exit(0)
    # except mon
    parser = CInitParser(shop_id=102,
                         shop_config_file="shopaccesoriibarbati.json",
                         parser_config=config)
    parser.start()

    print"--- {0} seconds ---".format(time.time() - startime)












