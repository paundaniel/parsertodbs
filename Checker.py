import collections
import json
import workerForRedis

__author__ = 'daniel'


class CChecker():
    # TODO:
    # 1. list all "to_insert" products
    # 2. list all "to_delete" products
    # 3. insert all products from list (if any)
    # 4. delete all products from list (if any)
    #
    # for statistics:
    #     count all inserted, deleted, updated, products

    def __init__(self, shop_id, timestamp, parser_config):
        self._shop_id = shop_id
        self._timestamp = timestamp
        self._redis = workerForRedis.CRedisHandler(shop_id=shop_id, parser_config=parser_config)
        self._mongo = self._redis._mongo

    def check_product(self, product):
        json_obj = collections.OrderedDict(sorted(product.items()))
        redis_entry = workerForRedis.CRedisEntry(json_obj=json.dumps(json_obj),
                                                 product_id=json_obj['idInShop'],
                                                 shop_id=self._shop_id)
        self._redis.redis_checking(redis_entry=redis_entry,
                                   timestamp=self._timestamp)


    def thigs_after_product_checking(self):
        self._redis.pipe_exec()
        self._mongo.execute_bulk()

        self._redis.check_for_deleted_products(shop_id=self._shop_id)
        self._redis.rename_active_products(shop_id=self._shop_id)

        self._redis.pipe_exec()
        self._mongo.execute_bulk()

    def exec_partial_bulk(self):
        self._mongo.execute_bulk()

    def get_parse_history(self):
        parse_history = self._redis._parse_history
        parse_history["timestamp"] = self._timestamp
        return parse_history

